import Vue from 'vue'
import App from './App.vue'
import Admin from './components/Admin.vue'
import 'bootstrap';
import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;
import 'popper.js';
import './assets/app.scss';
import VueRouter from 'vue-router';
import Dash from './components/views/Dash.vue'
import ManageEvent from './components/views/ManageEvent.vue'
import ManageUser from './components/views/ManageUser.vue'
import ManageBooking from './components/views/ManageBooking.vue'
import ManageVendor from './components/views/ManageVendor.vue'
import PendingRequest from './components/views/PendingRequest.vue'
import SkeletonCards from 'vue-ultimate-skeleton-cards';
Vue.use(SkeletonCards);

Vue.use(VueRouter);

const routes = [
    {path:'/', component: Dash},
    {path:'/ManageEvent', component: ManageEvent},
    {path:'/ManageBooking', component: ManageBooking},
    {path:'/ManageUser', component: ManageUser},
    {path:'/ManageVendor', component: ManageVendor},
    {path:'/PendingRequest', component: PendingRequest}

];

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  router,
  render: h => h(Admin)
}).$mount('#app')


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
